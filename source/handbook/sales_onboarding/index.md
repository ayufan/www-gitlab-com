---
layout: markdown_page
title: "Sales Onboarding"
---

* Have your manager grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs. In this folder, familiarize yourself with:

1. [Our Sales Process](https://docs.google.com/document/d/1F0vXw58ctLfk9LKrh35kOSjYvdah4skGGUt46l1-4GM/edit)

1. [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)

1. [The Sales Sheet](https://docs.google.com/spreadsheets/d/1755SblMccalWXSahspOrfzBwjGp4F8TkwlB8dOXCGlU/edit#gid=11) - add any targets to the Target tab

1. [Our Email Templates](https://docs.google.com/document/d/12X6Cvjwe_jvFi41gvXMLlq91lDgh5oSrRFdQEuEwhl8/edit#heading=h.ulpke0bgadag)

1. [Battlecards](https://docs.google.com/document/d/1zRIvk4CaF3FtfLfSK2iNWsG-znlh64GNeeMwrTmia_g/edit)

1. Our [Basic](https://docs.google.com/a/gitlab.com/document/d/19sjaBytIQeyIiyjXvFpM6QXTDdZNUTO_tQLowlIT_c4/edit), [Standard](https://docs.google.com/document/d/10Ur4nkiyr-qSdgDEVhFkLsoz5RPven8T-1eFmWY26bQ/edit) and Plus Subscription quotes

1. [Our Sales Communication Guide](https://docs.google.com/document/d/1IMDzTj3hZrnsA417z9Ye7WBa8yLkWxGzaLZNJ3O_nVA/edit#heading=h.3nffcmsbeqo7)

* Setup [Streak](https://www.streak.com) with Gmail - Streak is our CRM and Pipeline management tool:

1. Create a custom view for the current month

1. Create a view for all your deals

* Have your manager grant access to our accounting / finance apps [Recurly](https://app.recurly.com/login) and [Twinfield](https://login.twinfield.com/authentication/identity/login?ReturnUrl=%2fauthentication%2fissue%2fwsfed%3fwa%3dwsignin1.0%26wtrealm%3dhttps%253a%252f%252flogin.twinfield.com%26wctx%3drm%253d0%2526id%253dpassive%2526ru%253d%25252f%26wct%3d2015-04-17T21%253a56%253a53Z&wa=wsignin1.0&wtrealm=https%3a%2f%2flogin.twinfield.com&wctx=rm%3d0%26id%3dpassive%26ru%3d%252f&wct=2015-04-17T21%3a56%3a53Z). Ask a colleague if they can do a screenshare the next time they process an order.

* [Support and development process](/handbook/support_and_development_process)

* [Giving a GitLab demo](https://about.gitlab.com/handbook/demo/)
